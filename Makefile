ROOT_DIR:=$(shell pwd $(dirname $(firstword $(MAKEFILE_LIST))))

GO_VERSION ?= 1.23.4
GO_DIR = ${ROOT_DIR}/build/dependencies/go
GO_ROOT = ${GO_DIR}/go
GO_PATH = ${GO_DIR}/packages
GO = ${GO_ROOT}/bin/go

OTELCOL_BUILDER_VERSION ?= 0.115.0
OTELCOL_BUILDER_DIR = ${ROOT_DIR}/build/dependencies/ocb
OTELCOL_BUILDER = ${OTELCOL_BUILDER_DIR}/builder

UPX_VERSION ?= 4.2.4
UPX_DIR = ${ROOT_DIR}/build/dependencies/upx
UPX = ${UPX_DIR}/upx

ARCH ?= amd64,arm64
IMAGE_NAME ?= otelcol-kafka

generate-sources: ocb go
	@./scripts/generate-sources.sh -b ${OTELCOL_BUILDER} -p ${GO_PATH}

build-bin: go upx
	@./scripts/build-binary.sh -g ${GO} -p ${GO_PATH} -a ${ARCH} -u ${UPX}

build-image-verify:
	@./scripts/build-image-podman.sh -i ${IMAGE_NAME}

build-image-release:
	@./scripts/build-image-podman.sh -i ${IMAGE_NAME} -p true

.PHONY: go
go:
	[ ! -x ${GO} ] || exit 0; \
	set -e ;\
	os=$$(uname | tr A-Z a-z) ;\
	machine=$$(uname -m) ;\
	[ "$${machine}" != x86 ] || machine=386 ;\
	[ "$${machine}" != x86_64 ] || machine=amd64 ;\
	[ "$${machine}" != aarch64 ] || machine=arm64 ;\
	echo "Installing go ${GO_VERSION} ($${os}/$${machine}) at ${GO_DIR}";\
	mkdir -p ${GO_DIR} ;\
	curl -sL https://go.dev/dl/go${GO_VERSION}.$${os}-$${machine}.tar.gz | tar -xz -C ${GO_DIR} ;\

.PHONY: ocb
ocb: go
	[ ! -x ${OTELCOL_BUILDER} ] || exit 0; \
	set -e ;\
	echo "Installing ocb ${OTELCOL_BUILDER_VERSION} at ${OTELCOL_BUILDER_DIR}";\
	mkdir -p ${OTELCOL_BUILDER_DIR} ;\
	GOBIN=${OTELCOL_BUILDER_DIR} ${GO} install go.opentelemetry.io/collector/cmd/builder@v${OTELCOL_BUILDER_VERSION};\

.PHONY: upx
upx:
	[ ! -x ${UPX} ] || exit 0; \
	set -e ;\
	os=$$(uname | tr A-Z a-z) ;\
	machine=$$(uname -m) ;\
	[ "$${machine}" != x86 ] || machine=386 ;\
	[ "$${machine}" != x86_64 ] || machine=amd64 ;\
	[ "$${machine}" != aarch64 ] || machine=arm64 ;\
	echo "Installing upx ${UPX_VERSION} ($${os}/$${machine}) at ${UPX_DIR}";\
	mkdir -p ${UPX_DIR} ;\
	curl -sL https://github.com/upx/upx/releases/download/v${UPX_VERSION}/upx-${UPX_VERSION}-$${machine}_$${os}.tar.xz | tar -xJ --strip-components 1 -C ${UPX_DIR} ;\