#!/bin/bash
set -e
REPO_DIR="$(pwd "$(dirname ${BASH_SOURCE[0]})/..")"
SRC_DIR="${REPO_DIR}/build/generated-sources"
BUILD_BASE_DIR="${REPO_DIR}/build/bin"
GO=''
ARCH=''
UPX=''

while getopts g:p:a:u: flag
do
    case "${flag}" in
        g) GO=${OPTARG};;
        p) GOPATH=${OPTARG};;
        a) ARCH=${OPTARG};;
        u) UPX=${OPTARG};;
        *) exit 1;;
    esac
done

[[ -n "$GO" ]] || GO='go'
if [[ -z "$ARCH" ]]; then 
  MACHINE="$(uname -m)"
  if [ "${MACHINE}" = x86_64 ]; then
    ARCH="amd64"
  elif [ "${MACHINE}" = aarch64 ]; then
    ARCH="arm64"
  else
    ARCH="amd64"
  fi
fi
[[ -n "$UPX" ]] || UPX='upx'

echo "Using Go: $(command -v "$GO")"
echo "Using UPX: $(command -v "$UPX")"

for current_arch in ${ARCH//,/ }; do
  build_dir="${BUILD_BASE_DIR}/${current_arch}"
  echo "Build binary for ${current_arch} in ${build_dir}"
  mkdir -p "${build_dir}"
  (
    cd "${SRC_DIR}"
    GOPATH="${GOPATH}" CGO_ENABLED=0 GOOS=linux GOARCH=${current_arch} GO111MODULE=on ${GO} build -a -o "${build_dir}" -ldflags "-s -w -extldflags -static" -trimpath
    if [ $? = 0 ]; then
        echo "✅ SUCCESS: binary for ${current_arch} built in ${build_dir}."
    else
        echo "❌ ERROR: failed to build binary for ${current_arch}."
        exit 1
    fi
  )

  echo "Compress binary for ${current_arch} in ${build_dir}"
  ${UPX} -6 ${build_dir}/*
  # Decompressin at start up takes time and requires CPU time.
  # Its a compromise between small file size and decrompression time and CPU usage.
  # Better compression can be achieved with "--best --lzma".
  # File size is ~7MB instead of ~9MB, but start took ~400ms instead of ~100ms (without compression ~32MB file size and ~12ms start time)
done