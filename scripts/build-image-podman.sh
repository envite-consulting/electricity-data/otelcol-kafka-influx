#!/bin/bash
set -e
REPO_DIR="$(pwd "$(dirname ${BASH_SOURCE[0]})/..")"
SRC_DIR="${REPO_DIR}/src"
BINARY_BASE_DIR="${REPO_DIR}/build/bin"
DIST_DIR="${REPO_DIR}/build/dist"
IMAGE_NAME=''
PUSH=''

while getopts i:p: flag
do
    case "${flag}" in
        i) IMAGE_NAME=${OPTARG};;
        p) PUSH=${OPTARG};;
        *) exit 1;;
    esac
done

[[ -n "$IMAGE_NAME" ]] || IMAGE_NAME="$(basename ${REPO_DIR})"
[[ -n "$IMAGE_NAME" ]] || PUSH='false'

VERSION="$(git describe --tags --abbrev=7 || echo "v0.0.0-$(git rev-parse --short HEAD)")"

echo "Cleanup images for repository ${IMAGE_NAME}"
for image_id in $(buildah images -q --filter="reference=${IMAGE_NAME}:*"); do
  buildah rmi -f ${image_id}
done

echo "Create manifest for ${IMAGE_NAME}:${VERSION}"
buildah manifest create ${IMAGE_NAME}:${VERSION}
for directory in ${BINARY_BASE_DIR}/*/; do
  arch=$(basename $directory)

  echo "Build image ${IMAGE_NAME}:${VERSION} for ${arch}"
  mkdir -p "${DIST_DIR}/${arch}"
  cp -R "${BINARY_BASE_DIR}/${arch}/." "${DIST_DIR}/${arch}/"
  cp -R "${SRC_DIR}/." "${DIST_DIR}/${arch}/"
  buildah bud \
    --platform linux/${arch} \
    -t ${IMAGE_NAME}:${VERSION}-${arch} \
    -f "${SRC_DIR}/Containerfile" \
    "${DIST_DIR}/${arch}"
  buildah manifest add ${IMAGE_NAME}:${VERSION} ${IMAGE_NAME}:${VERSION}-${arch}
done

if [ "${PUSH}" = "true" ]; then
  echo "Push image ${IMAGE_NAME}:${VERSION}"
  buildah manifest push --all ${IMAGE_NAME}:${VERSION} docker://${IMAGE_NAME}:${VERSION}
fi
