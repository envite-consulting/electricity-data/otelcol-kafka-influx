#!/bin/bash
set -e
REPO_DIR="$(pwd "$(dirname ${BASH_SOURCE[0]})/..")"
SRC_DIR="${REPO_DIR}/src"
BUILD_DIR="${REPO_DIR}/build/generated-sources"
BUILDER=''

while getopts b:g:p: flag
do
    case "${flag}" in
        b) BUILDER=${OPTARG};;
        p) GOPATH=${OPTARG};;
        *) exit 1;;
    esac
done

[[ -n "$BUILDER" ]] || BUILDER='ocb'

mkdir -p "${BUILD_DIR}"

echo "Using Builder: $(command -v "$BUILDER")"

eval "echo \"$(cat "${SRC_DIR}/manifest.yaml")\"" > "${BUILD_DIR}/manifest.yaml"

GOPATH="${GOPATH}" "$BUILDER" --skip-compilation=true --config "${BUILD_DIR}/manifest.yaml" > "${BUILD_DIR}/build.log" 2>&1
if [ $? = 0 ]; then
    echo "✅ SUCCESS: generated code for distribution in ${BUILD_DIR}."
else
    echo "❌ ERROR: failed to generate code for distribution."
    echo "🪵 Build logs"
    echo "----------------------"
    cat "${BUILD_DIR}/build.log"
    echo "----------------------"
    exit 1
fi
